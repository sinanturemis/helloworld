FROM golang:alpine as build-image
RUN apk update && apk add git curl gcc musl-dev
RUN apk add -U --no-cache ca-certificates
RUN apk add --no-cache tzdata
WORKDIR /go/src/gitlab.trendyol.com/delivery/cosmos/helloworld
ADD . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -mod vendor -tags musl -installsuffix cgo -ldflags="-w -s -extldflags '-static'" -o /app/helloworld

FROM scratch
COPY --from=build-image /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-image /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build-image /app/helloworld /app/helloworld
COPY --from=build-image /go/src/gitlab.trendyol.com/delivery/cosmos/helloworld/config /app/config
ENV TZ=Europe/Istanbul
ENTRYPOINT ["/app/helloworld"]