GO      = go
GOFMT	= gofmt

TUFLAGS	:= -v -mod vendor -count 2
TCFLAGS	:= -v -mod vendor -cover -covermode=count -coverprofile=coverage.out -json > report.json

GOPKGS 	?= $(shell $(GO) list ./... | grep -v vendor/)

M 		= $(shell printf "\033[34;1m▶\033[0m")

.PHONY: test-unit
test-unit:  ; $(info $(M) Testing...)
	@$(GO) test $(GOPKGS) $(TUFLAGS)

.PHONY: test-coverage
test-coverage:  ; $(info $(M) Coveraging...)
	@$(GO) test $(GOPKGS) $(TCFLAGS)
